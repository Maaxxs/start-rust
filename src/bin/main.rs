/* Ignore the following warnings */
#[allow(unused_variables,dead_code)]



/// This is the main function documentation 
/// which won't be noticed since it is not public
fn main() {
    let a = 100;    // all vars are inmutable. Type i32
    let mut b = 5;  // mutable

    let c: i64 = 45;    // signed 64 bit int
    let d: u64 = 1337;     // unsigned 64 bit
    let e: f32 = 1337.1337;     // signed float 32 bit

    let update = true;
    let upgrade: bool = true;

    if update {
        println!("Update is truuu: {}", update);
    } else if upgrade{
        println!("Upgrade is truuuu: {}", upgrade);
    }

    if update && upgrade {
        println!("update and upgrade are true.");
    }

    println!("A float number: {}. An integer: {}", e, d);

    loop {
        b += 1;
        println!("{}", b);
        
        if b == 10 {break}
    }

    while b > 0 {
        print!("{}", b);
        b -= 1;
    }
    
    /* for loop witch numbers */
    println!();
    for i in 1..11 {
        println!("i is {}", i);
    }

    /* for loop over list */
    let list = vec!["Herbert", "Klaus", "Hans"];
    for (index, name) in list.iter().enumerate() {
        println!("At index {} the name is {}", index, name);
    }

    /* enum use */
    enum Direction {
        Up,
        Down,
        Left,
        Right
    }

    let player_direction:Direction = Direction::Left;

    /* works like a switch statement */
    match player_direction {
        Direction::Up => println!("We're heading up!!"),
        Direction::Left => println!("We're heading left!!"),
        Direction::Down => println!("We're heading left!!"),
        Direction::Right => println!("We're heading left!!"),
    }

    /* Create a tuple. Types can be different */
    let mut tuple = (4, 5.5, "hello world");
    println!("Index 2 is {}", tuple.2);   

    tuple.0 = 10;

    /* destruct a tuple */
    let (x,y,z) = tuple;
    println!("x and y are {} and {}", x, y);

    /* Create a array
     * Allocated on the stack
     * Types must be the same
     * not as flexible as vectors
     * not allowed to shrink/grow in size
     */
    let arr = [1,2,3,4,5,6];
    let strong: [i32; 4] = [0,1,2,3];
    let first = arr[0];
    /* out of bond access to array does not produce a compilation error, but
     * at runtime the check 'index < lenghtOfArray' will be performed and 
     * leads to a panic 'index out of bounds'.
     */

    /* Functions
     * declare return type
     * return early with 'return' otherwise the last expression is returned
     */
    fn add(a: i32, b:i32) -> i32 {
        /* Don't put a semicolon there -> will be a statement
         * without a semicolon it's an expression, which can be returned or
         * assigned to a variable
         */
        a + b   /* the result is returned implicitly */
    }
    println!("6 + 5 is {}", add(6,5));

    fn inc(x: i32) -> i32 {
        x + 1
    }
    println!("Increment 5 by one is {}", inc(5));

    // if else is an expression
    // all possible values for number must be of the same type!
    let number = if true { 5 } else { 10 };
    println!("Number is {}", number);


    /* Loops 
     * let result = loop{}
     * while true {}
     * for  {}
     */
    let mut counter = 0;
    let result = loop {
        counter += 1;
        println!("{} hi", counter);
        if counter == 10 {
            break counter;
        }
    };
    assert_eq!(result, 10);

    println!("Elements of array arr");
    for element in arr.iter() {
        println!("{}", element);
    }

    println!("Countdown from 3 to 1");
    for i in (1..4).rev() {
        println!("{}", i);
    }
    
    fn next_birthday(name: &str, age: u32) {
        let aged = age + 1;
        println!("{} is going to be {} on his/her next birthday.", name, aged);
    }

    next_birthday("Max", 100);
    
    // tuple do not have an .iter() method
    // because the type can be different
    let tup = (23, 3.14, "Max");
    let coll = (1,2,3,4,5);
    let arr = [1,2,3,4];
    for i in arr.iter() {
        println!("{}", i);
    }

    // x and y are coordinates
    let x = 0;
    let y = 0;
    match (x,y) {
        (0,0) => println!("You're in the Ursprung!"),
        (0,_) | (_,0) => {
            println!("You're moving either on the x or y axis.");
            println!("This is really awesome!")
            },
        _ => println!("you're somewhere ....")
        // _ => {}  // cover all, but don't do anything
    }



}

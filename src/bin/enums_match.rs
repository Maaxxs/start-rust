
#[derive(Debug)]
enum Message {
    Quit,                       // no data associated
    Write(String),              // a single string
    Move { x: i32, y: i32 },    // an anonymous struct
    ChangeColor(i32, i32, i32), // 3 int values
}


impl Message {
    fn call(&self) {
        println!("Message: {:?}", self);
    }
}

fn plus_one(number: Option<i32>) -> Option<i32> {
    match number {
        None => None,
        Some(number) => Some(number + 1),
    }
}

fn is_three(number: u8) -> bool {
    match number {
        3 => true,
        _ => false,
    }
}


fn main() {
    let hey = Message::Write(String::from("Hello Enum"));
    println!("{:?}", hey);
    hey.call();

    // mate is of type None
    let mate: Option<i32> = None;

    let x = Some(5);
    let y = plus_one(x);
    println!("x={:?}, y={:?}", x, y);

    println!("3 == 3 {}", is_three(3));

    // short variation of match.
    // Used if you only want to cover one case of many
    let some_u8_value = Some(3u8);
    if let Some(3) = some_u8_value {
        println!("is three");
    }

}




mod sound {
    pub mod instrument {
        pub fn clarinet() {
            // user super for relative path beginning at parent of clarinet()
            super::breathe_in();
            println!("lululu");
        }
    }

    fn breathe_in() {
        println!("Breathing in");
    }

    mod voice {

    }
}


fn main() {
    // Absolute path
    crate::sound::instrument::clarinet();

    // relative path
    sound::instrument::clarinet();

    call_vegetables_example();
}


mod plant {
    // struct can be public, but still have private fields
    pub struct Vegetable {
        pub name: String,       // public field
        id: u32,                // private field
    }

    // all variants of enum are public, if enum public
    pub enum Friend {
        Andi,
        Moritz,
        Franz
    }

    impl Vegetable {
        pub fn new(name: &str) -> Vegetable {
            Vegetable {
                name: String::from(name),
                id: 1,
            }
        }
    }
}


fn call_vegetables_example() {
    let mut v = plant::Vegetable::new("Tomato");

    v.name = String::from("Raspberry");
    println!("{} are delicious!", v.name);

    // this wont work 
    // println!("{} are delicious!", v.id);
}


use std::fmt::Result;
// the following would not work, but we can 'rename' it
// as it would be defined twice
// use std::io::Result;
use std::io::Result as IoResult;

// importen multiple items
use std::{cmp::Ordering, io};

// bring io and io::Write into scope
// use std::io::{self, Write};

// bring all public items into scope
use std::collections::*;




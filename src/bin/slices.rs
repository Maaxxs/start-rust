/* Build a function which gets a string and returns 
 * the first word in that string. Return the whole
 * string if it is one word.
 */

/* The type str is a slice by default
 * String can be represented as slice by &String[..]
 */

fn main() {
    let s = String::from("Hello gorgeous world");
    let empty = "";
    
    // pass a String slice to the function
    let first_word = find_word(&s[..]);
    let from_empty = find_word(&empty);

    println!("The first word of '{}' is '{}'", s, first_word);
    println!("The first word of '{}' is '{}'", empty, from_empty);

    if empty.is_empty() {
        println!("The string empty is empty");
    }

}

fn find_word(string: &str) -> &str {
    let bytes = string.as_bytes();
    for (index, &byte) in bytes.iter().enumerate() {
        if byte == b' ' {
            return &string[..index]
        }    
    }    
    // no space found; the string is one word
    &string[..]
}

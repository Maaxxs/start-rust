/* Build a function, which takes an argument, pluralize the noun, and 
 * return that noun, so that both Strings are still valid in main() and
 * can be printed.
*/

fn main() {
    let single = String::from("book");

    let multiple = pluralize(&single);

    println!("I have on {}, you have two{}", single, multiple);
}

fn pluralize(s: &String) -> String {
    let mut multiple = s.clone();
    multiple.push_str("s");
    multiple
}

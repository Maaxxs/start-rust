// implement Debug trait, so we can use the debug marco {:?} to print
// complex structures, arrays ...
#[derive(Debug)]
struct User {
    name: String,
    email: String,
    logins: u64,
    active: bool
}


fn main() {
    let mut user = build_user(String::from("Max"), String::from("fun@greet.de"));

    println!("{:?}", user);
    user.name = String::from("Bill");
    println!("{:?}", user);

    user.logins += user.logins;
    // struct update syntax
    let user2 = User {
        name: String::from("Bill Gates"),
        email: String::from("club@mate.de"),
        ..user // use the not explicitly set field values from struct user
    };
    println!("{:?}", user2);

    // tuple structs. structs without names
    struct Point(i32, i32, i32);
    let origin = Point(0, 0, 0);
    println!("Point({}, {}, {})", origin.0, origin.1, origin.2);

    let tup = ("eins", 2, 3.3);
    println!("{:?}", tup);

    // Unit-like structs. Unit type: ()
    // useful, when you need to implement a atrait on some type, but don't have
    // any data that you want to store in the type itself.

    // using structs. Rectangle
    do_rectangle();

}

fn build_user(name: String, email: String) -> User {
    /*
    User {
        name: name,
        email: email,
        logins: 1,
        active: true,
    }
    */
    // shorter: if params and struct fields have the same name
    User {
        name,
        email,
        logins: 1,
        active: true,
    }
}

#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
    //area: u32,
}

// define a method on that struct
impl Rectangle {
    // methods can also take ownership of self and we just want to borrow self
    // so we just want a reference to self
    fn area(&self) -> u32 {
        self.length * self.width
    }

    // if we want to change a value, we use a mutable ref
    fn change_length(&mut self, length: u32) {
        self.length = length;
    }
}

fn do_rectangle() {

    let len = 5;
    let wid = 5;
    let mut form = Rectangle{
        length: len,
        width: wid,
    };

    println!("Values of struct: {:?}", form);
    // or on bigger structs
    println!("Values of struct: {:#?}", form);  // one entry per line
    println!("Area: {}", area(&form));

    println!("Change length to 7");
    form.change_length(7);
    println!("Call the method 'area' on struct Rectangle. Value: {}", form.area());
}

fn area(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.length
}

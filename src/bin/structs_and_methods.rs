#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
    //area: u32,
}

// Multiple impl block for one struct are possible
impl Rectangle {
    // method
    fn area(&self) -> u32 {
        self.width * self.length
    }
    // defining a method on Rectangle
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.length > other.length && self.width > other.width
    }

    // defining a associated function on Rectangle
    fn square(length: u32) -> Rectangle {
        Rectangle {
            length: length,
            width: length,
        }
    }
}


fn main() {
    let rect1 = Rectangle { length: 30, width: 50 };
    let rect2 = Rectangle { length: 10, width: 40 };
    let rect3 = Rectangle { length: 60, width: 45 };

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));

    let square = Rectangle::square(10);
    println!("Len of squre={} and area={}",
            square.length, square.area());
}

